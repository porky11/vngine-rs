# VNgine - The Visual Novel Engine 

[![Crates.io](https://img.shields.io/crates/v/vngine)](https://crates.io/crates/vngine)

A Rust-powered visual novel engine with multi-linear storytelling, scene customization, and real-time state management.

## Features

- 📖 Multi-format story progression (linear, multilinear, petri nets)
- 🎨 Dynamic styling system with cascading overrides
- 🖼️ Layered image composition with positioning
- ⏱️ Customizable timing and basic animation controls
- 💬 Character-specific dialog configurations
- 🔄 Full state revert capabilities in arbitrary order

## Quickstart

You can run the example project using this command:

```
cargo run -- examples/simple/simple.vng
```

## Formats

There are multiple files needed:

* the project file (`.vng`)
* the story (`.pk`)
* _optional_ the structure (as one of the following):
  * order
  * multilinear (`.mld`)
  * petri net (`.pn`, `.pnk`)

You might also have a look at the [simple example project](examples/simple).

### The project file

This file has a line based format representing a key-value list. The list is separated by a space.
It has multiple lines like this:

```
key1 value1
key2 value2
key3 value3
```

Different kinds of keywords belong to different namespaces.

There are two ways to define key based on namespaces.

Specify paths directly:

```
Namespace:key1 value1
Namespace:key2 value2
Namespace:key3 value3
```

Specify namespace using headers:

```
# Namespace

key1 value1
key2 value2
key3 value3
```

Namespaces can be nested.

A path like `Path:To:Namespace:key` could be set to `value` like this:

```
# Path

## To

### Namespace

key value
```

Currently keys in these namespaces exist:

- static:
  - `Path`
  - `Size`
  - `Font`
  - `Layer`
- configurable:
  - `Color`
  - `Timing`
  - `Layout`
  - `Image`
  - `Object`
  - `Character`

Configurable namespaces can be overridden.

Don't forget to look at [the example](examples/simple/simple.vng).

#### Path

These keys specify the file paths.
They are followed the a file path.

These files are supported:

- `story`: path of the story file or directory
- `order`: path to a file containing the file paths in a specific order when using a linear story structure
- `net`: path to the net when using a petri net based story structure
- `names`: path to the file which contains the transition names of the net when using a petri net based story structure
- `multilinear`: path of the structure when using a multilinear story structure
- `save`: path to save the progress to
- `image`: path from which images are loaded

They default to the name of the project file (could be `project.vng`), but with an appropriate extension (`project.pk`, `project.mld`, `project.sav`).

#### Size

The default size of the image using the `size` key.

It's specified like this:

* `size w h`
* `size w h factor`

`factor` is optional and useful if you want to specify the ratio, but make it easy to change the scale.

For example both of these sizes represent full HD:

* `size 1920 1080`
* `size 16 9 120`

Besides that, you specify positions in terms of `w` and `h`, so if you change `factor`, all the UI elements will be scaled, too.

#### Layer

Layer lets you define, which layers exist.

Each layer can contain a single object consisting of multiple images at a time.

You can give your layers names. They will appear in order and always stay the same.

By default, there are these two layers:

- `Background`
- `Character`

They will appear as sub namespaces of `Object`.

#### Color

The color keys are used to specify the color of a specific UI element.

These color keys are currently supported:

* `background`
* `foreground`
* `dialog-text-fill`
* `dialog-text-line`
* `dialog-name-fill`
* `dialog-name-line`
* `choice-default-text-fill`
* `choice-default-text-line`
* `choice-default-name-fill`
* `choice-default-name-line`
* `choice-select-text-fill`
* `choice-select-text-line`
* `choice-select-name-fill`
* `choice-select-name-line`
* `revert-default-text-fill`
* `revert-default-text-line`
* `revert-default-name-fill`
* `revert-default-name-line`
* `revert-select-text-fill`
* `revert-select-text-line`
* `revert-select-name-fill`
* `revert-select-name-line`

By default, all outlines (`line`) are white and all fillings (`fill`) and the background (`background`) are black.

A color can either be a color name or a hexadecimal representation.

These color names are available:

* `black`
* `white`
* `red`
* `green`
* `blue`
* `yellow`
* `cyan`
* `magenta`

The hexadecimal representation can have different numbers of digits:

* 1 or 2 digits => gray value
* 3 or 6 digits => RGB value
* 4 or 8 digits => RGBA value

#### Font

There are also keys to specify alternative fonts.

You can specify up to eight different fonts, which all may or may not have these three features:

* `bold`
* `italic`
* `monospace`

The specific keys are these:

* `default`
* `default-bold`
* `default-italic`
* `default-bold-italic`
* `mono`
* `mono-bold`
* `mono-italic`
* `mono-bold-italic`

#### Image

Probably one of the most important features of visual novels are images.

The image path is specified relative to the project path.

Each subnamespace you define here will be converted into an object.

Every Object consists of multiple layers.

You could define an object like this:

```
# Image

## Character

body Body.png
clothes Clothes.png
accessories
```

This will define an object called `Character` consisting of three images `body`, `clothes` and `accessories`.
The `accessories` are empty by default, but can be overridden.

#### Object

You can add different objects. There will be one sub namespace for each layer, that exists.

By default there are these two subnamespaces in this order (from back to front):

- `Background`
- `Character`

Each of them has these parameters:

- `image`: The namespace name of an image defined in `Images`.
- `hor`: The horizontal position.
- `ver`: The vertical position.
- `scale`: The scale.

#### Timing

These specify different timings (in seconds):

* `auto-next`: The time until the next dialog box is shown when auto play is active (default: 1.0s).
* `letter`: The time until the next letter appears (default: 0.05s).
* `line`: The time until the next line appears (default: 0.5s).
* `select` The duration of the animaton when selecting a choice (default: 0.5s).

#### Layout

The layout keys are used to specify the layout of a specific UI element.

It uses some subnamespaces:

- `Main`: Layout of the main text box.
- `List`: Layout of the selection boxes.
- `View`: The view of the camera.

`Main` and `List` both support the same keys:

* `hor`: The horizontal position.
* `ver`: The vertical position.
* `width`: The horizontal size.
* `height`: The vertical size.
* `corner`: The radius of the rounded corners..
* `font-size`: The unmodified font size in this box

The `View` supports different keys:

* `hor`: The horizontal position.
* `ver`: The vertical position.
* `zoom`: The zoom.

#### Character

This namespace contains custom subordinate namespaces for each character.

Each of these namespaces representing a character will have a single `name` parameter, which represents the actually displayed name.
This might make changes to character names easier.
If undefined, the written name will be used.

Each character will also have the sub namespaces, everything from the list of configurable namespaces, besides characters themselves:

- `Color`
- `Timing`
- `Layout`
- `Image`
- `Object`

If a person speaks, all the settings of the speaking character will be used.
For example each person could have a default character image to be shown, when they speak, and have their own color settings for text boxes.

### The structure

The structure can currently be defined using different systems:
- linear story
- multilinear
- petri nets

#### Linear

The story file will be read from start to end, even if there are headers and subheaders.

When specifying a directory, the order in which the scenes are player is undefined.
Scenes in the same file will still 

But you can specify the order using the `order` path.
This way you can specify in which order the files should be played.
You just need to specify the full path for each file.

#### Multilinear

The [multilinear format](https://gitlab.com/porky11/multilinear) allows you to specify the story in terms of conditions of events and channel specific value changes.

You can use arbitrary names for channels, events and values.

A channel can represent anything and can only hold a single value.
By default, each channel has no value, so empty names are allowed.

You only have to specify the `multilinear` path.
This will point to a multilinear definition (`.mld`), which specifies all these events.

A simple conversation in this format could look like this:

```mld
# Talk to girl

place: school
relationship to girl: unknown > known
talking: > active

## Good answer

relationship to girl: known > liked
talking: active >

## Bad answer

relationship to girl: known > hated
talking: active >
```

There have to be compatible headers in the text files.
So each of these events can be represented by a dialog.

For more information on the described format, have a look at [the parser](https://gitlab.com/porky11/multilinear-parser).

#### Petri nets

You can also use petri nets to define the story structure.

**Warning** - *Support might be dropped in future releases*

You have to specify two paths:
- `net` - a `.pn` file, the binary representation of a petri net
- `names` - a `.pnk` file, which specifies the names of the petri net transitions, which should have the same names as the story events

For creating petri nets, you can use [the petri net editor](https://gitlab.com/porky11/petri-net-editor).
But you can also just use [the core library](https://gitlab.com/porky11/pns) or [its safe Rust wrapper](https://gitlab.com/porky11/pnrs) directly to create them programmatically.

### The story

The story file uses dialogi(https://gitlab.com/porky11/dialogi), a line based file format inspired by markdown.

#### Headers

Headers start with an arbitrary number of `#`. The number of `#` is called the level.
Every header of a level higher than one is a subheader of last header of one level less.

There can be multiple subheaders of the same name as long as they are not subheaders of the same header.

Example:

```
# Header 1

## Subheader 1

### Subheader 1

### Subheader 2

## Subheader 2

# Header 2

## Subheader 1

## Subheader 2
```


It's invalid if there is a subheader after a header, which is more than one level higher.

This is not allowed:

```
# Header 1

## Subheader level 2

# Header 2

### Subheader level 3
```

The story file can also be a directory. In this case it always has to be specified manually in the project file.
In this case, it will search for the story files in that directory and use their titles as headers.

The first header example could be converted to this, when wanting to use multiple files instead:

File `Header 1.pk`

```
# Subheader 1

## Subheader 1

## Subheader 2

# Subheader 2
```

File `Header 2.pk`:

```
# Subheader 1

# Subheader 2
```

#### Lines, blocks and segments

Every text between two headers belongs to the header before it and is called a segment.

Every line will represent a line in the text box.
The content of a single text box is represented by a text block, multiple lines separated by at least one newline.

The first line of the first text block of a segment will be the name of the choice.

You can add a name followed by a colon (`Name:`) before a text block to indicate someone saying the text.
The name might be in the same line or in its own line.

Choices can have speaker names, too.

#### Changes

During a text segment, you can change settings.
These changes will be active until the end of the segment and after that be reverted to the default settings as defined in the project file.

The changes can occur before a block, between two lines of a block and when the block is finished.

Changes use most of the same settings as the project file.
Currently all settings for `Color`, `Timing`, `Image` and `Layout` are supported.
`Character` are supported as well, overriding the character specific settings.
But in this file, they are prefixed by a `-`.

You can also set an reset some setting to the default value by adding a `!` after it.

In the following example, the background color will be set red and the dialog fill color to white.
When the first text line is written, the background color will be set to green and when both lines are shown, the background is set back to the default value.
The dialog fill color will be set back to the default color after the segment.

Here's an example:

```
- Color:background red
- Color:dialog-fill white
First text line.
- Color:background green
Second text line.
- Color:background!
- Color:dialog-fill!

Girl:
- Character:name Alice
- Character:dialog-fill green
Hey!

Boy:
- Character:name Bob
- Character:dialog-fill blue
Hello!
How are you?

Girl: Thanks, I'm fine.
```

The girl will still be called Alice and have a green text box the second time she says something.

Changes can also be defined hierarchical by adding a colon (`:`) at the end of a line and putting the sub path with an indentation of 2 into the next line.

The previous example can be simplified like this:

```
- Color:
  - background red
  - dialog-fill white
First text line.
- Color:background green
Second text line.
- Color:
  - background!
  - dialog-fill!

Girl:
- Character:
  - name Alice
  - dialog-fill green
Hey!

Boy:
- Character:
  - name Bob
  - dialog-fill blue
Hello!
How are you?

Girl: Thanks, I'm fine.
```

#### Formatting

Lines can be formatted by using [pukram](https://gitlab.com/porky11/pukram-formatting).

You can add formatting by writing a special symbol. Everything after this symbol will be formatted that way.
A second one of the same special symbol will remove the formatting again.
At the end of a line, all formatting is reverted to normal, so if you want to have the complete line formatted in a specific way, just add the symbol in the beginning of the line.

These formattings are supported:

- `*` bold
- `/` italic
- `` ` `` monospace
- `^` superscript
- `|` subscript
- `_` underscore
- `~` strikethrough

All formattings can be combined.
Combining superscript and subscript (`^|`) will result in small text.

You can ignore formattings and display the characters instead by adding a `\` before them.

## Overrides

All the defined settings have a priority:

1. scene settings
2. character specific scene settings
3. character specific default settings
4. default settings

## Usage

### Opening the project

Just open the project file.

On command line, that's done this way:

```
vngine [project.vng]
```

### Controls

You can control the game using the mouse or the arrow keys.

The keyboard controls are inspired by video players.

There are three basic actions:
* `confirm`
* `revert`
* `select`

You press the left mouse button or the right arrow key to `confirm` and advance the story.

You press the right mouse button or the left arrow key to `revert` to the previous state.

When there are multiple choices, you can `select` the choice by moving the cursor or using the arrow keys up and down.

If the choice is a normal choice, you use the `confirm` action to confirm your choice.
The `revert` action reverts to the previous state.

But since choices can be reverted in different orders, there are also reversion choices.
In that case, you use the `revert` action to confirm your choice, since it reverts the story.
The `confirm` action still advances the story.

## Planned features

- a switch to WGPU as renderer
- overrides to settings, which are driven by the story structure, so that choices will globally affect everything
- custom definitions for the look of text boxes using a simple vector graphics format
- support for vector grapics based characters and backgrounds
- support for 3D characters and backgrounds
- basic audio support
- proper animation support
- editor capabilities while playing

## Support

If you want to support me, send monero to this address:

`84aFCRxU6evdf17V1xTKYTBAPPgKra1pbgT2RoayFzrjFzra5TTPw22CcoJrUXcRN93wJqnUsT59Jerr876rBYUpHPchLpR`
