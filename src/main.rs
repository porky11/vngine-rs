mod utilities;

mod player;
mod simulation;
mod start;

use petri_net_simulation::PetriNetSimulation;
use simulation::LinearSimulation;
use start::start;

use header_config::{Error, parse_config};
use multilinear::MultilinearSimulation;
use rfd::FileDialog;

use std::path::PathBuf;

fn main() {
    let mut args = std::env::args();
    args.next();
    let path = if let Some(config_path) = args.next() {
        if args.next().is_some() {
            println!("Too many arguments!\nUsage: vngine [config_path]");
            return;
        }
        PathBuf::from(config_path)
    } else {
        FileDialog::new()
            .add_filter("VNgine", &["vng"])
            .pick_file()
            .expect("Invalid file")
    };

    let mut config_map = match parse_config(&path) {
        Ok(map) => map,
        Err(err) => {
            match err {
                Error::OpeningFile => eprintln!("Opening file {path:?} failed"),
                Error::SubheaderWithoutHeader => {
                    eprintln!("File {path:?} has a subheader without a header")
                }
                Error::MultipleKeys(key) => eprintln!("Path {path:?} has a duplicate of key {key}"),
                Error::Input => eprintln!("Input error while reading {path:?}"),
            };
            return;
        }
    };

    let multilinear = config_map.shift_remove("Path:multilinear");
    let net = config_map.shift_remove("Path:net");
    let names = config_map.shift_remove("Path:names");
    let order = config_map.shift_remove("Path:order");

    match (multilinear, net, names, order) {
        (Some(filepath), None, None, None) => {
            let multilinear_path = path.with_file_name(filepath.as_ref());
            start::<1, MultilinearSimulation>(config_map, path, [&multilinear_path]);
        }
        (None, Some(net_path), Some(names_path), None) => {
            let net_path = path.with_file_name(net_path.as_ref());
            let names_path = path.with_file_name(names_path.as_ref());
            start::<2, PetriNetSimulation>(config_map, path, [&net_path, &names_path]);
        }
        (None, None, None, order) => {
            if let Some(order_path) = order {
                let order_path = path.with_file_name(order_path.as_ref());
                start::<1, LinearSimulation>(config_map, path, [&order_path])
            } else {
                start::<0, LinearSimulation>(config_map, path, [])
            }
        }
        _ => println!("Invalid set of paths defined."),
    }
}
