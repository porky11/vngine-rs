mod state;
mod text_box;

use femto_formatting::FontPack;
use state::{ChoiceBox, GameState, PlayDirection};
use text_box::{AppearProgress, TextBox};
use vn_settings::{Change, Parameter, PlayerSettings, ViewLayoutSettings};

use crate::{
    player::text_box::TextBoxStyle,
    utilities::{color, lerp},
};

use data_stream::{
    FromStream, ToStream, default_settings::PortableSettings, from_stream, to_stream,
};
use dialogi::DialogSequence;
use event_simulation::Simulation;
use femtovg::{Canvas, ImageFlags, ImageId, Paint, Path, Renderer};
use gapp::{Render, Update};
use gapp_winit::WindowInput;
use simple_color::Color;
use winit::{
    event::{
        ElementState, KeyboardInput, ModifiersState, MouseButton, VirtualKeyCode, WindowEvent,
    },
    event_loop::ControlFlow,
};

use std::{
    collections::{BTreeMap, HashMap},
    fs::File,
    io::{Error, ErrorKind},
    path::PathBuf,
};

struct AdvanceTextData<'a> {
    settings: &'a mut PlayerSettings,
    time: &'a mut f32,
    progress: &'a mut Option<AppearProgress>,
    block: &'a mut usize,
    dialog_sequence: &'a DialogSequence<Change, Parameter>,
    states: &'a mut HashMap<Parameter, usize>,
}

impl AdvanceTextData<'_> {
    fn next(self) -> bool {
        *self.time = 0.0;
        if let Some(AppearProgress { lines, .. }) = *self.progress {
            *self.progress = None;

            let block = &self.dialog_sequence.blocks[*self.block];
            for text_line in &block.lines[(lines + 1)..] {
                for parameter in &text_line.actions {
                    if let Some(state) = self.states.get_mut(parameter) {
                        let change = &self.dialog_sequence.changes[parameter][*state];
                        self.settings.change(change);
                        *state += 1;
                    }
                }
            }

            for parameter in &block.final_actions {
                if let Some(state) = self.states.get_mut(parameter) {
                    let change = &self.dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
        } else {
            *self.block += 1;

            if *self.block >= self.dialog_sequence.blocks.len() {
                self.settings.reset();
                return false;
            }

            let block = &self.dialog_sequence.blocks[*self.block];
            let (parameters, new_progress) = if block.lines.is_empty() {
                (&block.final_actions, None)
            } else {
                (
                    &block.lines[0].actions,
                    Some(AppearProgress { lines: 0, chars: 0 }),
                )
            };

            *self.progress = new_progress;

            for parameter in parameters {
                if let Some(state) = self.states.get_mut(parameter) {
                    let change = &self.dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
        }

        true
    }

    fn prev(self) -> bool {
        if *self.block == 0 {
            self.settings.reset();
            return false;
        }

        let block = &self.dialog_sequence.blocks[*self.block];

        if let Some(AppearProgress { lines, .. }) = *self.progress {
            for text_line in block.lines[0..=lines].iter().rev() {
                for parameter in &text_line.actions {
                    let Some(state) = self.states.get_mut(parameter) else {
                        continue;
                    };

                    *state -= 1;
                    if *state == 0 {
                        let parameter = &self.dialog_sequence.changes[parameter][0].parameter();
                        self.settings.revert(parameter);
                    } else {
                        let change = &self.dialog_sequence.changes[parameter][*state - 1];
                        self.settings.change(change);
                    }
                }
            }
        } else {
            for parameter in &block.final_actions {
                let Some(state) = self.states.get_mut(parameter) else {
                    continue;
                };

                *state -= 1;
                if *state == 0 {
                    let parameter = &self.dialog_sequence.changes[parameter][0].parameter();
                    self.settings.revert(parameter);
                } else {
                    let change = &self.dialog_sequence.changes[parameter][*state - 1];
                    self.settings.change(change);
                }
            }

            for text_line in block.lines.iter().rev() {
                for parameter in &text_line.actions {
                    let Some(state) = self.states.get_mut(parameter) else {
                        continue;
                    };

                    *state -= 1;
                    if *state == 0 {
                        let parameter = &self.dialog_sequence.changes[parameter][0].parameter();
                        self.settings.revert(parameter);
                    } else {
                        let change = &self.dialog_sequence.changes[parameter][*state - 1];
                        self.settings.change(change);
                    }
                }
            }
        }

        *self.time = 0.0;
        *self.progress = None;
        *self.block -= 1;

        true
    }
}

pub struct Fonts {
    pub default: FontPack,
    pub mono: FontPack,
}

pub struct RenderContext<R: Renderer> {
    canvas: Canvas<R>,
    pub image_path: PathBuf,
    pub image_cache: HashMap<Box<str>, Option<ImageId>>,
    pub fonts: Fonts,
}

impl<R: Renderer> RenderContext<R> {
    pub fn new(canvas: Canvas<R>, image_path: PathBuf, fonts: Fonts) -> Self {
        let mut image_cache: HashMap<_, Option<_>> = HashMap::new();
        image_cache.insert("".into(), None);

        Self {
            canvas,
            image_path,
            image_cache,
            fonts,
        }
    }

    fn image(&mut self, id: &str) -> Option<ImageId> {
        if let Some(&image) = self.image_cache.get(id) {
            return image;
        }

        let mut path = self.image_path.clone();
        path.push(id);
        let image = match self.canvas.load_image_file(path, ImageFlags::empty()) {
            Ok(image) => Some(image),
            Err(err) => {
                eprintln!("Error loading image {id:?}: {err:?}");
                None
            }
        };
        self.image_cache.insert(id.into(), image);
        image
    }
}

pub struct Player<T: Simulation> {
    simulation: T,
    texts: BTreeMap<T::Event, DialogSequence<Change, Parameter>>,
    cursor: (f32, f32),
    size: (f32, f32),
    clip_size: (f32, f32),
    scale: f32,
    state: GameState<T::Event>,
    auto_play: bool,
    save_path: PathBuf,
    settings: PlayerSettings,
    view: ViewLayoutSettings<f32>,
}

impl<T: Simulation> Player<T> {
    pub fn new(
        size: (f32, f32),
        simulation: T,
        texts: BTreeMap<T::Event, DialogSequence<Change, Parameter>>,
        save_path: PathBuf,
        settings: PlayerSettings,
    ) -> Self {
        Self {
            simulation,
            texts,
            cursor: (0.0, 0.0),
            size,
            clip_size: size,
            scale: 1.0,
            state: GameState::Start,
            auto_play: false,
            save_path,
            settings,
            view: ViewLayoutSettings {
                hor: 0.0,
                ver: 0.0,
                zoom: 1.0,
            },
        }
    }

    fn text_front(&mut self, sequence: T::Event) {
        let dialog_sequence = &self.texts[&sequence];

        let changes = &dialog_sequence.changes;
        let mut states = HashMap::new();
        for parameter in changes.keys() {
            states.insert(parameter.clone(), 0);
        }

        let block = &dialog_sequence.blocks[0];

        let progress = if dialog_sequence.blocks[0].lines.len() > 1 {
            for parameter in &block.lines[0].actions {
                if let Some(state) = states.get_mut(parameter) {
                    let change = &dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
            Some(AppearProgress { lines: 0, chars: 0 })
        } else {
            for parameter in &block.final_actions {
                if let Some(state) = states.get_mut(parameter) {
                    let change = &dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
            None
        };

        self.state = GameState::Text {
            sequence,
            block: 0,
            states,
            time: 0.0,
            hidden: false,
            progress,
        };
    }

    fn text_selected(&mut self, sequence: T::Event, mut states: HashMap<Parameter, usize>) {
        let dialog_sequence = &self.texts[&sequence];

        let progress = if dialog_sequence.blocks[0].lines.len() > 1 {
            for parameter in &dialog_sequence.blocks[0].lines[1].actions {
                if let Some(state) = states.get_mut(parameter) {
                    let change = &dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
            Some(AppearProgress { lines: 1, chars: 0 })
        } else {
            for parameter in &dialog_sequence.blocks[0].final_actions {
                if let Some(state) = states.get_mut(parameter) {
                    let change = &dialog_sequence.changes[parameter][*state];
                    self.settings.change(change);
                    *state += 1;
                }
            }
            None
        };

        self.state = GameState::Text {
            sequence,
            block: 0,
            states,
            time: 0.0,
            hidden: false,
            progress,
        }
    }

    fn text_back(&mut self, sequence: T::Event) {
        let dialog_sequence = &self.texts[&sequence];

        let changes = &dialog_sequence.changes;
        let mut states = HashMap::new();
        for (parameter, changes) in changes {
            states.insert(parameter.clone(), changes.len());
            let change = &changes[changes.len() - 1];
            self.settings.change(change);
        }

        self.state = GameState::Text {
            sequence,
            block: dialog_sequence.blocks.len() - 1,
            states,
            time: 0.0,
            hidden: false,
            progress: None,
        };
    }

    fn load_text(&mut self, sequence: T::Event, block: usize) {
        let dialog_sequence = &self.texts[&sequence];
        let dialog_len = dialog_sequence.blocks.len();
        let block = if block < dialog_len {
            block
        } else {
            dialog_len - 1
        };

        let changes = &dialog_sequence.changes;
        let mut states = HashMap::new();

        for parameter in changes.keys() {
            states.insert(parameter.clone(), 0);
        }

        for block in &dialog_sequence.blocks[0..=block] {
            for line in &block.lines {
                for parameter in &line.actions {
                    if let Some(state) = states.get_mut(parameter) {
                        *state += 1;
                    }
                }
            }

            for parameter in &block.final_actions {
                if let Some(state) = states.get_mut(parameter) {
                    *state += 1;
                }
            }
        }

        for (parameter, changes) in changes {
            let Some(state) = states.get(parameter) else {
                continue;
            };

            if *state > 0 {
                let change = &changes[*state - 1];
                self.settings.change(change);
            }
        }

        self.state = GameState::Text {
            sequence,
            block,
            states,
            time: 0.0,
            hidden: false,
            progress: None,
        };
    }

    fn next_text(&mut self) -> bool {
        let transitions: Vec<_> = self
            .simulation
            .callables()
            .filter_map(|sequence| {
                let text = &self.texts.get(&sequence)?;
                if text.blocks.is_empty() {
                    None
                } else {
                    Some(sequence)
                }
            })
            .collect();

        if transitions.len() == 1 {
            let sequence = transitions[0];
            unsafe { self.simulation.call(sequence) }
            self.text_front(sequence);
            return false;
        }

        let mut selected = None;
        let boxes: Vec<_> = transitions
            .into_iter()
            .enumerate()
            .map(|(i, sequence)| {
                let dialog_sequence = &self.texts[&sequence];
                let name = &dialog_sequence.blocks[0].name;

                let list = &self.settings.layout.list;
                let choice_box = ChoiceBox {
                    text_box: TextBox::from_settings(list, name)
                        .with_offset(0.0, (i + 1) as f32 * list.height.get(name) * 2.0),
                    sequence,
                };
                if choice_box
                    .text_box
                    .contains(self.cursor.0 / self.scale, self.cursor.1 / self.scale)
                {
                    selected = Some(i);
                }
                choice_box
            })
            .collect();

        self.state = if boxes.is_empty() {
            GameState::End
        } else {
            GameState::Choice {
                boxes,
                direction: PlayDirection::Forward,
                selected,
            }
        };

        true
    }

    fn prev_text(&mut self) -> bool {
        if let GameState::Text { sequence, .. } = &self.state {
            unsafe { self.simulation.revert(*sequence) }
        }

        let transitions: Vec<_> = self
            .simulation
            .revertables()
            .filter_map(|sequence| {
                let text = &self.texts.get(&sequence)?;
                if text.blocks.is_empty() {
                    None
                } else {
                    Some(sequence)
                }
            })
            .collect();

        if transitions.len() == 1 {
            let sequence = transitions[0];
            self.text_back(sequence);
            return false;
        }

        let mut selected = None;
        let boxes: Vec<_> = transitions
            .into_iter()
            .enumerate()
            .map(|(i, sequence)| {
                let dialog_sequence = &self.texts[&sequence];
                let name = &dialog_sequence.blocks[0].name;

                let list = &self.settings.layout.list;
                let choice_box = ChoiceBox {
                    text_box: TextBox::from_settings(list, name)
                        .with_offset(0.0, (i + 1) as f32 * list.height.get(name) * 2.0),
                    sequence,
                };
                if choice_box
                    .text_box
                    .contains(self.cursor.0 / self.scale, self.cursor.1 / self.scale)
                {
                    selected = Some(i);
                }
                choice_box
            })
            .collect();

        self.state = if boxes.is_empty() {
            GameState::Start
        } else {
            GameState::Choice {
                boxes,
                direction: PlayDirection::Backward,
                selected,
            }
        };

        true
    }

    fn advance(&mut self, direction: PlayDirection, skip: bool) {
        use GameState::*;
        match &mut self.state {
            Start => {
                if direction == PlayDirection::Backward {
                    return;
                }

                while !self.next_text() && skip {
                    self.settings.reset();
                }
            }

            End => {
                if direction == PlayDirection::Forward {
                    return;
                }

                while !self.prev_text() && skip {
                    self.settings.reset();
                }
            }

            Selecting {
                sequence, states, ..
            } => {
                let sequence = *sequence;
                let states = states.clone();
                self.text_selected(sequence, states);
            }

            Text {
                sequence,
                block,
                states,
                time,
                hidden,
                progress,
            } => {
                if *hidden {
                    *hidden = false;
                    return;
                }

                let dialog_sequence = &self.texts[sequence];

                if skip {
                    use PlayDirection::*;
                    match direction {
                        Forward => while !self.next_text() {},
                        Backward => while !self.prev_text() {},
                    }
                    self.settings.reset();
                    return;
                }

                let data = AdvanceTextData {
                    settings: &mut self.settings,
                    time,
                    progress,
                    block,
                    dialog_sequence,
                    states,
                };

                use PlayDirection::*;
                match direction {
                    Forward => {
                        if data.next() {
                            return;
                        }
                        self.next_text();
                    }
                    Backward => {
                        if data.prev() {
                            return;
                        }
                        self.prev_text();
                    }
                }
            }

            Choice {
                boxes,
                direction: current_direction,
                selected,
            } => {
                if direction != *current_direction {
                    use PlayDirection::*;
                    let advance = match direction {
                        Forward => Self::next_text,
                        Backward => Self::prev_text,
                    };
                    while !advance(self) && skip {
                        self.settings.reset();
                    }

                    return;
                }

                let Some(selected) = selected else {
                    return;
                };
                let selected = *selected;

                let choice_box = boxes.swap_remove(selected);

                use PlayDirection::*;
                match direction {
                    Forward => {
                        unsafe { self.simulation.call(choice_box.sequence) }
                        if skip {
                            while !self.next_text() {
                                self.settings.reset();
                            }

                            return;
                        }

                        let dialog_sequence = &self.texts[&choice_box.sequence];

                        let changes = &dialog_sequence.changes;
                        let mut states = HashMap::new();
                        for parameter in changes.keys() {
                            states.insert(parameter.clone(), 0);
                        }

                        let block = &dialog_sequence.blocks[0];

                        for parameter in &block.lines[0].actions {
                            if let Some(state) = states.get_mut(parameter) {
                                let change = &dialog_sequence.changes[parameter][*state];
                                self.settings.change(change);
                                *state += 1;
                            }
                        }

                        let ChoiceBox { text_box, sequence } = choice_box;

                        self.state = Selecting {
                            text_box,
                            sequence,
                            states,
                            ratio: 0.0,
                        };
                    }
                    Backward => {
                        let sequence = choice_box.sequence;
                        self.text_back(sequence);
                        if !skip {
                            return;
                        }

                        while !self.prev_text() {
                            self.settings.reset();
                        }
                    }
                }
            }
        }
    }

    fn save(&self)
    where
        T::AccessData: ToStream<PortableSettings>,
        T::Event: ToStream<PortableSettings>,
    {
        match File::create(&self.save_path) {
            Ok(mut file) => {
                if let Err(err) = to_stream(self.simulation.data(), &mut file) {
                    eprintln!("Failed to write simulation: {err:?}")
                } else if let Err(err) = self.state.save(&mut file) {
                    eprintln!("Failed to write state: {err:?}")
                }
            }
            Err(err) => eprintln!("Failed to create file: {err:?}"),
        }
    }

    fn load_state(&mut self, file: &mut File) -> Result<(), Error>
    where
        T::LoadData: FromStream<PortableSettings>,
        T::Event: FromStream<PortableSettings>,
    {
        use GameState::*;
        match from_stream::<PortableSettings, u8, _>(file)? {
            0 => self.state = Start,
            1 => self.state = End,
            2 => {
                self.state = Start;
                self.advance(PlayDirection::Forward, false);
            }
            3 => {
                self.state = End;
                self.advance(PlayDirection::Backward, false);
            }
            4 => {
                let sequence = from_stream(file)?;
                let block = from_stream::<PortableSettings, _, _>(file)?;
                self.load_text(sequence, block);
            }
            _ => return Err(Error::new(ErrorKind::InvalidData, "Invalid state")),
        }

        Ok(())
    }

    fn load(&mut self)
    where
        T::LoadData: FromStream<PortableSettings>,
        T::Event: FromStream<PortableSettings>,
    {
        match File::open(&self.save_path) {
            Ok(mut file) => {
                match from_stream(&mut file) {
                    Ok(values) => {
                        if self.simulation.reload(values).is_err() {
                            eprintln!("Failed to reload simulation");
                            return;
                        }
                    }
                    Err(err) => {
                        eprintln!("Failed to read simulation: {err:?}");
                        return;
                    }
                }

                self.settings.reset();

                if let Err(err) = self.load_state(&mut file) {
                    eprintln!("Failed to read state: {err:?}")
                }
            }
            Err(err) => eprintln!("Failed to open file: {err:?}"),
        }
    }
}

impl<T: Simulation> Update for Player<T> {
    fn update(&mut self, timestep: f32) {
        use GameState::*;

        let name = if let Text { sequence, .. } | Selecting { sequence, .. } = &self.state {
            let dialog_sequence = &self.texts[sequence];
            let block = if let Text { block, .. } = &self.state {
                *block
            } else {
                0
            };
            let current_block = &dialog_sequence.blocks[block];
            &current_block.name
        } else {
            ""
        };

        let view_duration = self.settings.timing.view.get(name);
        let time_factor = 1.0 - 2.0f32.powf(-timestep / view_duration);

        let goal_zoom = self.settings.layout.view.zoom.get(name);
        let goal_hor = self.settings.layout.view.hor.get(name);
        let goal_ver = self.settings.layout.view.ver.get(name);

        self.view.zoom += (goal_zoom - self.view.zoom) * time_factor;
        self.view.hor += (goal_hor - self.view.hor) * time_factor;
        self.view.ver += (goal_ver - self.view.ver) * time_factor;

        match &mut self.state {
            Text {
                sequence,
                block,
                states,
                time,
                hidden,
                progress,
            } => {
                if *hidden {
                    return;
                }

                let dialog_sequence = &self.texts[sequence];
                let current_block = &dialog_sequence.blocks[*block];
                let name = &current_block.name;

                if let Some(AppearProgress { lines, chars }) = progress {
                    let current_lines = &current_block.lines;
                    let current_text = &current_lines[*lines].text;

                    *time += timestep;

                    let letter_time = self.settings.timing.letter.get(name);
                    if *time < letter_time {
                        return;
                    }

                    while *time >= letter_time {
                        *time -= letter_time;
                        *chars +=
                            unsafe { current_text[*chars..].chars().next().unwrap_unchecked() }
                                .len_utf8();

                        if *chars >= current_text.len() {
                            break;
                        }
                    }

                    if *chars < current_text.len() {
                        return;
                    }

                    *chars = 0;
                    *lines += 1;
                    *time -= self.settings.timing.line.get(name);

                    if *lines < current_lines.len() {
                        for parameter in &current_lines[*lines].actions {
                            if let Some(state) = states.get_mut(parameter) {
                                let change = &dialog_sequence.changes[parameter][*state];
                                self.settings.change(change);
                                *state += 1;
                            }
                        }

                        return;
                    }

                    for parameter in &current_block.final_actions {
                        if let Some(state) = states.get_mut(parameter) {
                            let change = &dialog_sequence.changes[parameter][*state];
                            self.settings.change(change);
                            *state += 1;
                        }
                    }

                    *time = 0.0;
                    *progress = None;
                } else if self.auto_play {
                    *time += timestep;

                    let auto_next_time = self.settings.timing.auto_next.get(name);

                    if *time < auto_next_time {
                        return;
                    }

                    *time -= auto_next_time;

                    self.advance(PlayDirection::Forward, false);
                }
            }

            Selecting {
                sequence,
                states,
                ratio,
                ..
            } => {
                let dialog_sequence = &self.texts[sequence];
                let current_block = &dialog_sequence.blocks[0];
                let name = &current_block.name;

                *ratio += timestep / self.settings.timing.select.get(name);
                if *ratio >= 1.0 {
                    let sequence = *sequence;
                    let states = states.clone();
                    self.text_selected(sequence, states);
                }
            }

            Choice { .. } => (),

            Start | End => (),
        }
    }
}

impl<T: Simulation> Player<T> {
    fn render_scene<R: Renderer>(&self, name: &str, context: &mut RenderContext<R>) {
        let canvas = &mut context.canvas;

        let (w, h) = (canvas.width(), canvas.height());
        canvas.clear_rect(0, 0, w, h, color(self.settings.colors.background.get(name)));

        let w = w as f32;
        let h = h as f32;

        let ViewLayoutSettings { hor, ver, zoom } = self.view;

        let hor = -hor * zoom * self.scale;
        let ver = -ver * zoom * self.scale;

        let sw = self.scale * self.clip_size.0;
        let sh = self.scale * self.clip_size.1;
        let sratio = sw / sh;

        for object in &self.settings.objects.objects {
            let Some(image_set) = object.image.get(name) else {
                continue;
            };

            let hor = hor + object.hor.get_ref(name) * zoom * self.scale;
            let ver = ver + object.ver.get_ref(name) * zoom * self.scale;
            let zoom = zoom * object.scale.get_ref(name);

            for image in &self.settings.images.images[image_set] {
                let Some(image) = image.get_ref(name) else {
                    continue;
                };

                let Some(image) = context.image(image) else {
                    continue;
                };

                let info = context.canvas.image_info(image).expect("Invalid image");

                let w = info.width() as f32;
                let h = info.height() as f32;
                let ratio = w / h;

                let (w, h) = if ratio > sratio {
                    (sw, sw / ratio)
                } else {
                    (sh * ratio, sh)
                };
                let w = w * zoom;
                let h = h * zoom;

                let x = hor - w / 2.0;
                let y = ver - h / 2.0;

                let mut path = Path::new();
                path.rect(x, y, w, h);

                let paint = Paint::image(image, x, y, w, h, 0.0, 1.0);
                context.canvas.fill_path(&path, &paint);
            }
        }

        let mut path = Path::new();
        path.rect(-w / 2.0, -h / 2.0, w, h);
        let paint = Paint::color(color(self.settings.colors.foreground.get(name)));
        context.canvas.fill_path(&path, &paint);
    }
}

impl<T: Simulation, R: Renderer> Render<RenderContext<R>> for Player<T> {
    fn render(&self, context: &mut RenderContext<R>) {
        use GameState::*;
        match &self.state {
            Choice {
                boxes,
                selected,
                direction,
            } => {
                self.render_scene("", context);

                use PlayDirection::*;
                let colors = match direction {
                    Forward => &self.settings.colors.select_box,
                    Backward => &self.settings.colors.revert_box,
                };
                for (i, choice_box) in boxes.iter().enumerate() {
                    let colors = if &Some(i) == selected {
                        &colors.selected
                    } else {
                        &colors.default
                    };

                    let text = &self.texts[&choice_box.sequence].blocks[0];

                    choice_box.text_box.render(
                        text.lines(),
                        self.settings.names.get(&text.name),
                        TextBoxStyle {
                            text_line_color: colors.text_line.get(&text.name),
                            text_fill_color: colors.text_fill.get(&text.name),
                            name_line_color: colors.name_line.get(&text.name),
                            name_fill_color: colors.name_fill.get(&text.name),
                        },
                        Some(&AppearProgress { lines: 1, chars: 0 }),
                        context,
                        self.scale,
                    );
                }
            }

            Selecting {
                text_box,
                sequence,
                ratio,
                ..
            } => {
                let dialog_sequence = &self.texts[sequence];
                let current_block = &dialog_sequence.blocks[0];
                let name = &current_block.name;

                self.render_scene(name, context);

                fn lerp_color(a: &Color, b: &Color, ratio: f32) -> Color {
                    Color::rgba(
                        lerp(a.r as f32, b.r as f32, ratio) as u8,
                        lerp(a.g as f32, b.g as f32, ratio) as u8,
                        lerp(a.b as f32, b.b as f32, ratio) as u8,
                        lerp(a.a as f32, b.a as f32, ratio) as u8,
                    )
                }

                let ratio = (*ratio * std::f32::consts::PI / 2.0).sin();

                let dialog_colors = &self.settings.colors.dialog_box;
                let select_colors = &self.settings.colors.select_box.selected;

                let text_line_color = lerp_color(
                    &select_colors.text_line.get(name),
                    &dialog_colors.text_line.get(name),
                    ratio,
                );
                let text_fill_color = lerp_color(
                    &select_colors.text_fill.get(name),
                    &dialog_colors.text_fill.get(name),
                    ratio,
                );
                let name_line_color = lerp_color(
                    &select_colors.name_line.get(name),
                    &dialog_colors.name_line.get(name),
                    ratio,
                );
                let name_fill_color = lerp_color(
                    &select_colors.name_fill.get(name),
                    &dialog_colors.name_fill.get(name),
                    ratio,
                );

                text_box
                    .lerp(
                        &TextBox::from_settings(&self.settings.layout.main, name),
                        ratio,
                    )
                    .render(
                        current_block.lines(),
                        self.settings.names.get(name),
                        TextBoxStyle {
                            text_line_color,
                            text_fill_color,
                            name_line_color,
                            name_fill_color,
                        },
                        Some(&AppearProgress { lines: 1, chars: 0 }),
                        context,
                        self.scale,
                    );
            }

            Text {
                sequence,
                block,
                hidden,
                progress,
                ..
            } => {
                let dialog_sequence = &self.texts[sequence];
                let current_block = &dialog_sequence.blocks[*block];
                let name = &current_block.name;

                self.render_scene(name, context);

                if !hidden && !current_block.lines.is_empty() {
                    let colors = &self.settings.colors.dialog_box;
                    TextBox::from_settings(&self.settings.layout.main, name).render(
                        current_block.lines(),
                        self.settings.names.get(name),
                        TextBoxStyle {
                            text_line_color: colors.text_line.get(name),
                            text_fill_color: colors.text_fill.get(name),
                            name_line_color: colors.name_line.get(name),
                            name_fill_color: colors.name_fill.get(name),
                        },
                        progress.as_ref(),
                        context,
                        self.scale,
                    );
                }
            }

            Start | End => self.render_scene("", context),
        }

        context.canvas.flush();
    }
}

impl<T: Simulation, R: Renderer> WindowInput<ModifiersState, RenderContext<R>> for Player<T>
where
    T::LoadData: FromStream<PortableSettings>,
    T::AccessData: ToStream<PortableSettings>,
    T::Event: FromStream<PortableSettings> + ToStream<PortableSettings>,
{
    fn input(
        &mut self,
        event: &WindowEvent<'_>,
        control_flow: &mut ControlFlow,
        modifiers: &mut ModifiersState,
        context: &mut RenderContext<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(new_modifiers) => *modifiers = new_modifiers,

            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

            WindowEvent::Resized(size) => {
                let canvas = &mut context.canvas;
                canvas.set_size(size.width, size.height, 1.0);
                let (w, h) = (size.width as f32, size.height as f32);
                self.size = (w, h);
                let (cw, ch) = self.clip_size;
                let (sw, sh) = (w / cw, h / ch);
                self.scale = if sw < sh { sw } else { sh };
                canvas.reset_transform();
                canvas.translate(w / 2.0, h / 2.0);
            }

            WindowEvent::CursorMoved { position, .. } => {
                let (x, y) = (
                    position.x as f32 - self.size.0 / 2.0,
                    position.y as f32 - self.size.1 / 2.0,
                );
                self.cursor = (x, y);

                let GameState::Choice {
                    boxes, selected, ..
                } = &mut self.state
                else {
                    return;
                };

                *selected = None;
                for (i, choice_box) in boxes.iter().enumerate() {
                    if choice_box.text_box.contains(x / self.scale, y / self.scale) {
                        *selected = Some(i);
                        break;
                    }
                }
            }

            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(keycode),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => {
                use VirtualKeyCode::*;
                match keycode {
                    Left => self.advance(PlayDirection::Backward, modifiers.ctrl()),
                    Right => self.advance(PlayDirection::Forward, modifiers.ctrl()),
                    Up => self.state.up(),
                    Down => self.state.down(),
                    Space => self.auto_play = !self.auto_play,
                    _ => (),
                }
                if modifiers.ctrl() {
                    match keycode {
                        S => self.save(),
                        O => self.load(),
                        _ => (),
                    }
                }
            }

            WindowEvent::MouseInput { button, state, .. } => {
                use MouseButton::*;
                let direction = match button {
                    Left => PlayDirection::Forward,
                    Right => PlayDirection::Backward,
                    _ => return,
                };

                use ElementState::*;
                match state {
                    Pressed => (),
                    Released => return,
                }

                self.advance(direction, modifiers.ctrl());
            }

            _ => (),
        }
    }
}
