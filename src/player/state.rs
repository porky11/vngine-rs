use super::{AppearProgress, TextBox};

use vn_settings::Parameter;

use data_stream::{ToStream, default_settings::PortableSettings, to_stream};

use std::{collections::HashMap, fs::File, io::Error};

#[derive(Debug)]
pub struct ChoiceBox<S> {
    pub text_box: TextBox,
    pub sequence: S,
}

#[derive(Debug, PartialEq, Eq)]
pub enum PlayDirection {
    Forward,
    Backward,
}

#[derive(Debug)]
pub enum GameState<S> {
    Start,
    End,
    Selecting {
        text_box: TextBox,
        sequence: S,
        states: HashMap<Parameter, usize>,
        ratio: f32,
    },
    Text {
        sequence: S,
        block: usize,
        states: HashMap<Parameter, usize>,
        time: f32,
        hidden: bool,
        progress: Option<AppearProgress>,
    },
    Choice {
        boxes: Vec<ChoiceBox<S>>,
        direction: PlayDirection,
        selected: Option<usize>,
    },
}

impl<S> GameState<S> {
    pub fn up(&mut self) {
        use GameState::*;
        match self {
            Text { hidden, .. } => *hidden = false,
            Choice {
                boxes, selected, ..
            } => {
                if let Some(index) = selected {
                    if *index > 0 {
                        *index -= 1;
                    }
                } else if !boxes.is_empty() {
                    *selected = Some(0)
                }
            }
            _ => (),
        }
    }

    pub fn down(&mut self) {
        use GameState::*;
        match self {
            Text { hidden, .. } => *hidden = true,
            Choice {
                boxes, selected, ..
            } => {
                if let Some(index) = selected {
                    if *index < boxes.len() - 1 {
                        *index += 1;
                    }
                } else if !boxes.is_empty() {
                    *selected = Some(0)
                }
            }
            _ => (),
        }
    }
}

impl<S: ToStream<PortableSettings>> GameState<S> {
    pub fn save(&self, file: &mut File) -> Result<(), Error> {
        use GameState::*;
        match self {
            Start => to_stream::<PortableSettings, _, _>(&0u8, file)?,
            End => to_stream::<PortableSettings, _, _>(&1u8, file)?,
            Choice {
                direction: PlayDirection::Forward,
                ..
            } => to_stream::<PortableSettings, _, _>(&2u8, file)?,
            Choice {
                direction: PlayDirection::Backward,
                ..
            } => to_stream::<PortableSettings, _, _>(&3u8, file)?,
            Selecting { sequence, .. } => {
                to_stream::<PortableSettings, _, _>(&4u8, file)?;
                to_stream(sequence, file)?;
                to_stream::<PortableSettings, _, _>(&0usize, file)?;
            }
            Text {
                sequence, block, ..
            } => {
                to_stream::<PortableSettings, _, _>(&4u8, file)?;
                to_stream(sequence, file)?;
                to_stream::<PortableSettings, _, _>(block, file)?;
            }
        }

        Ok(())
    }
}
