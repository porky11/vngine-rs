use crate::utilities::{color, lerp};

use femto_formatting::render_partial_text_formatted;
use femtovg::{Align, Baseline, Paint, Path, Renderer};
use pukram_formatting::Formatting;
use simple_color::Color;
use vn_settings::{BoxLayoutSettings, Override};

use super::RenderContext;

#[derive(Debug)]
pub struct AppearProgress {
    pub lines: usize,
    pub chars: usize,
}

#[derive(Debug)]
pub struct TextBox {
    pub pos: (f32, f32),
    pub size: (f32, f32),
    pub corner: f32,
    pub line: f32,
    pub text_size: f32,
    pub name_size: f32,
}

impl TextBox {
    pub fn from_settings(main: &BoxLayoutSettings<Override<f32>>, name: &str) -> Self {
        Self {
            pos: (main.hor.get(name), main.ver.get(name)),
            size: (main.width.get(name), main.height.get(name)),
            corner: main.corner.get(name),
            line: main.line.get(name),
            text_size: main.text_size.get(name),
            name_size: main.name_size.get(name),
        }
    }

    pub fn with_offset(self, x: f32, y: f32) -> Self {
        Self {
            pos: (self.pos.0 + x, self.pos.1 + y),
            ..self
        }
    }

    pub fn contains(&self, x: f32, y: f32) -> bool {
        self.pos.0 - self.size.0 / 2.0 <= x
            && x <= self.pos.0 + self.size.0 / 2.0
            && self.pos.1 - self.size.1 / 2.0 <= y
            && y <= self.pos.1 + self.size.1 / 2.0
    }

    pub fn lerp(&self, other: &Self, ratio: f32) -> Self {
        let (x0, y0) = self.pos;
        let (w0, h0) = self.size;

        let (x1, y1) = other.pos;
        let (w1, h1) = other.size;

        Self {
            pos: (lerp(x0, x1, ratio), lerp(y0, y1, ratio)),
            size: (lerp(w0, w1, ratio), lerp(h0, h1, ratio)),
            corner: lerp(self.corner, other.corner, ratio),
            line: lerp(self.line, other.line, ratio),
            text_size: lerp(self.text_size, other.text_size, ratio),
            name_size: lerp(self.name_size, other.name_size, ratio),
        }
    }
}

pub struct TextBoxStyle {
    pub text_line_color: Color,
    pub text_fill_color: Color,
    pub name_line_color: Color,
    pub name_fill_color: Color,
}

impl TextBox {
    pub fn render<'a, R: Renderer>(
        &self,
        text: impl IntoIterator<Item = &'a str>,
        name: &str,
        style: TextBoxStyle,
        progress: Option<&AppearProgress>,
        context: &mut RenderContext<R>,
        scale: f32,
    ) {
        let &Self {
            pos: (x, y),
            size: (w, h),
            corner: r,
            line,
            text_size,
            name_size,
        } = self;

        let text_size = scale * text_size;

        let text_line_color = color(style.text_line_color);
        let text_line_stroke_paint = Paint::color(text_line_color).with_line_width(line * scale);
        let text_line_text_paint = Paint::color(text_line_color)
            .with_text_align(Align::Left)
            .with_text_baseline(Baseline::Top)
            .with_font_size(text_size);

        let (x, y) = (x * scale, y * scale);
        let (w, h) = (w * scale, h * scale);
        let (x0, y0) = (x - w / 2.0, y - h / 2.0);
        let r = r * scale;

        let mut path = Path::new();
        path.rounded_rect(x - w / 2.0, y - h / 2.0, w, h, r);

        let text_fill_paint = Paint::color(color(style.text_fill_color));

        context.canvas.fill_path(&path, &text_fill_paint);
        context.canvas.stroke_path(&path, &text_line_stroke_paint);

        let formatting = Formatting::default();

        let mut line_index = 0;
        for (i, line) in text.into_iter().enumerate() {
            let (tx, ty) = (
                x0 + r,
                y0 + r - text_size / 2.0 + line_index as f32 * text_size,
            );

            let mut last = false;
            let mut count = line.len();

            if let Some(AppearProgress { lines, chars }) = progress {
                if *lines == i {
                    last = true;
                    count = *chars;
                }
            }

            line_index += render_partial_text_formatted(
                tx,
                ty,
                w / 2.0 - r,
                line,
                count,
                formatting,
                text_size,
                &context.fonts.default,
                &context.fonts.mono,
                &text_line_text_paint,
                &mut context.canvas,
            );

            if last {
                break;
            }
        }

        if !name.is_empty() {
            let name_line_color = color(style.name_line_color);
            let name_line_stroke_paint =
                Paint::color(name_line_color).with_line_width(line * scale);
            let name_line_text_paint = Paint::color(name_line_color)
                .with_text_baseline(Baseline::Middle)
                .with_text_align(Align::Left)
                .with_font_size(scale * name_size);

            let w0 = context
                .canvas
                .measure_text(x0 + r / 2.0, y, name, &name_line_text_paint)
                .map(|metrics| metrics.width())
                .unwrap_or(0.0);

            let mut path = Path::new();
            path.rounded_rect(x0 + r, y0 - r / 2.0, r + w0, r, r / 2.0);

            let name_fill_paint = Paint::color(color(style.name_fill_color));

            context.canvas.fill_path(&path, &name_fill_paint);
            context.canvas.stroke_path(&path, &name_line_stroke_paint);

            let _ = context
                .canvas
                .fill_text(x0 + r * 1.5, y0, name, &name_line_text_paint);
        }
    }
}
