use vn_settings::{Change, Parameter, SettingsContext};

use dialogi::DialogSequence;
use event_simulation::Simulation;
use multilinear::{Event, MultilinearSimulation};
use multilinear_parser::{NamedMultilinearInfo, parse_multilinear};
use petri_net_simulation::PetriNetSimulation;
use pns::{Net, Tid};

use std::{
    collections::{BTreeMap, HashMap},
    fs::File,
    io::{BufRead, BufReader},
    path::Path,
};

pub trait LoadableSimulation<const SIZE: usize>: Simulation + Sized {
    fn load(
        paths: [&Path; SIZE],
        story_path: &Path,
        settings_context: &mut SettingsContext,
    ) -> (
        Self,
        BTreeMap<Self::Event, DialogSequence<Change, Parameter>>,
    );
}

pub struct LinearSimulation {
    state: usize,
    size: usize,
}

impl LinearSimulation {
    fn new(size: usize) -> Self {
        Self { state: 0, size }
    }
}

impl Simulation for LinearSimulation {
    type StateLoadingError = ();
    type AccessData = usize;
    type LoadData = usize;
    type Event = usize;
    type EventContainer<'a> = std::option::IntoIter<usize>;

    #[inline]
    fn data(&self) -> &usize {
        &self.state
    }

    fn reload(&mut self, state: usize) -> Result<(), ()> {
        self.state = state;
        Ok(())
    }

    fn callables(&self) -> std::option::IntoIter<usize> {
        (self.state < self.size).then_some(self.state).into_iter()
    }

    fn callable(&self, event: usize) -> bool {
        self.state == event
    }

    fn revertables(&self) -> std::option::IntoIter<usize> {
        (self.state > 0).then_some(self.state - 1).into_iter()
    }

    fn revertable(&self, event: usize) -> bool {
        self.state == event + 1
    }

    unsafe fn call(&mut self, event: usize) {
        self.state = event + 1;
    }

    unsafe fn revert(&mut self, event: usize) {
        self.state = event;
    }
}

impl LoadableSimulation<0> for LinearSimulation {
    fn load(
        []: [&Path; 0],
        story_path: &Path,
        settings_context: &mut SettingsContext,
    ) -> (Self, BTreeMap<usize, DialogSequence<Change, Parameter>>) {
        let text_list: Vec<_> = DialogSequence::map_from_path(story_path, settings_context)
            .expect("Error parsing dialog sequence");

        let mut texts = BTreeMap::new();

        for (i, text) in text_list.into_iter().enumerate() {
            texts.insert(i, text);
        }

        (Self::new(texts.len()), texts)
    }
}

impl LoadableSimulation<1> for LinearSimulation {
    fn load(
        [order_path]: [&Path; 1],
        story_path: &Path,
        settings_context: &mut SettingsContext,
    ) -> (Self, BTreeMap<usize, DialogSequence<Change, Parameter>>) {
        let mut text_list = Vec::new();
        let order_file = File::open(order_path).expect("Error opening order file");
        for line in BufReader::new(order_file).lines() {
            let line = line.expect("Error reading order file");
            let line = line.trim();
            if line.is_empty() {
                continue;
            }

            let mut path = story_path.to_path_buf();
            path.push(line);

            DialogSequence::fill_map_from_path(&path, &mut text_list, settings_context)
                .expect("Error parsing dialog sequence");
        }

        let mut texts = BTreeMap::new();

        for (i, text) in text_list.into_iter().enumerate() {
            texts.insert(i, text);
        }

        (Self::new(texts.len()), texts)
    }
}

impl LoadableSimulation<2> for PetriNetSimulation {
    fn load(
        [net_path, names_path]: [&Path; 2],
        story_path: &Path,
        settings_context: &mut SettingsContext,
    ) -> (Self, BTreeMap<Tid, DialogSequence<Change, Parameter>>) {
        let net = Net::load(net_path).expect("Error opening net file");
        let names_file = File::open(names_path).expect("Error opening multilinear file");

        let mut text_map: HashMap<_, _> =
            DialogSequence::map_from_path(story_path, settings_context)
                .expect("Error parsing dialog sequence");

        let mut texts = BTreeMap::new();
        for (tid, line) in net.transition_ids().zip(BufReader::new(names_file).lines()) {
            let Ok(line) = line else {
                continue;
            };

            let name: Vec<_> = line.trim().split('#').map(Into::into).collect();
            if let Some(text) = text_map.remove(&name) {
                texts.insert(tid, text);
            }
        }

        (Self::new(net), texts)
    }
}

impl LoadableSimulation<1> for MultilinearSimulation {
    fn load(
        [multilinear_path]: [&Path; 1],
        story_path: &Path,
        settings_context: &mut SettingsContext,
    ) -> (Self, BTreeMap<Event, DialogSequence<Change, Parameter>>) {
        let multilinear_file =
            File::open(multilinear_path).expect("Error opening multilinear file");
        let NamedMultilinearInfo { info, events, .. } =
            parse_multilinear(multilinear_file).expect("Error parsing Multilinear");

        let mut text_map: HashMap<_, _> =
            DialogSequence::map_from_path(story_path, settings_context)
                .expect("Error parsing dialog sequence");

        let texts = events
            .into_iter()
            .filter_map(|(event, name)| {
                let name: Vec<_> = name.into_iter().collect();
                let entry = text_map.remove(&name)?;
                Some((event, entry))
            })
            .collect();

        (Self::new(info), texts)
    }
}
