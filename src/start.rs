use crate::{
    player::{Fonts, Player, RenderContext},
    simulation::LoadableSimulation,
};

use data_stream::{FromStream, ToStream, default_settings::PortableSettings};
use dialogi::DialogParameter as _;
use femto_formatting::FontPack;
use femtovg::{Canvas, FontId, Renderer, renderer::OpenGl};
use glutin::{
    config::ConfigTemplateBuilder,
    context::{ContextApi, ContextAttributesBuilder},
    display::GetGlDisplay,
    prelude::*,
    surface::{SurfaceAttributesBuilder, WindowSurface},
};
use glutin_winit::DisplayBuilder;
use indexmap::IndexMap;
use raw_window_handle::HasRawWindowHandle;
use resource::resource;
use vn_settings::{
    ColorSettings, ImageSettings, LayoutSettings, Names, ObjectSettings, Parameter, PlayerSettings,
    SettingsContext, TimingSettings,
};
use winit::{event::ModifiersState, event_loop::EventLoop, window::WindowBuilder};

use std::{
    collections::HashMap,
    num::NonZeroU32,
    path::{Path, PathBuf},
};

pub fn start<const SIZE: usize, T: LoadableSimulation<SIZE> + 'static>(
    mut config_map: IndexMap<Box<str>, Box<str>>,
    path: PathBuf,
    simulation_paths: [&Path; SIZE],
) where
    T::LoadData: FromStream<PortableSettings>,
    T::AccessData: ToStream<PortableSettings>,
    T::Event: FromStream<PortableSettings> + ToStream<PortableSettings>,
{
    let story_path = config_map.shift_remove("Path:story").map_or_else(
        || path.with_extension("pk"),
        |filepath| path.with_file_name(filepath.as_ref()),
    );
    let save_path = config_map.shift_remove("Path:save").map_or_else(
        || path.with_extension("sav"),
        |filepath| path.with_file_name(filepath.as_ref()),
    );
    let image_path = config_map.shift_remove("Path:image").map_or_else(
        || path.clone(),
        |filepath| path.with_file_name(filepath.as_ref()),
    );

    let (width, height, factor) =
        config_map
            .shift_remove("Size:size")
            .map_or((16, 9, 120), |size| {
                let mut args = size.split(' ');
                let err = "Size not specified correctly";
                let w = args.next().expect(err).parse().expect(err);
                let h = args.next().expect(err).parse().expect(err);
                let factor = args.next().map_or(1, |factor| factor.parse().expect(err));
                (w, h, factor)
            });

    let (w, h) = (width * factor, height * factor);

    let event_loop = EventLoop::new();

    let (renderer, window, context, surface) = {
        let window_builder = WindowBuilder::new()
            .with_inner_size(winit::dpi::PhysicalSize::new(w, h))
            .with_title("VNgine");

        let template = ConfigTemplateBuilder::new().with_alpha_size(8);

        let display_builder = DisplayBuilder::new().with_window_builder(Some(window_builder));

        let (window, gl_config) = display_builder
            .build(&event_loop, template, |configs| {
                configs
                    .reduce(|accum, config| {
                        let transparency_check = config.supports_transparency().unwrap_or(false)
                            & !accum.supports_transparency().unwrap_or(false);

                        if transparency_check || config.num_samples() > accum.num_samples() {
                            config
                        } else {
                            accum
                        }
                    })
                    .expect("No valid config found")
            })
            .expect("Building window failed");

        let window = window.expect("No window created");

        let raw_window_handle = Some(window.raw_window_handle());

        let gl_display = gl_config.display();

        let context_attributes = ContextAttributesBuilder::new().build(raw_window_handle);
        let fallback_context_attributes = ContextAttributesBuilder::new()
            .with_context_api(ContextApi::Gles(None))
            .build(raw_window_handle);
        let mut not_current_gl_context = Some(unsafe {
            gl_display
                .create_context(&gl_config, &context_attributes)
                .unwrap_or_else(|_| {
                    gl_display
                        .create_context(&gl_config, &fallback_context_attributes)
                        .expect("failed to create context")
                })
        });

        let (width, height): (u32, u32) = window.inner_size().into();
        let raw_window_handle = window.raw_window_handle();
        let attrs = SurfaceAttributesBuilder::<WindowSurface>::new().build(
            raw_window_handle,
            NonZeroU32::new(width).expect("Width not zero"),
            NonZeroU32::new(height).expect("Height not zero"),
        );

        let surface = unsafe {
            gl_config
                .display()
                .create_window_surface(&gl_config, &attrs)
                .expect("Surface creation failed")
        };

        let gl_context = not_current_gl_context
            .take()
            .expect("No context found")
            .make_current(&surface)
            .expect("Failed to create context");

        let renderer =
            unsafe { OpenGl::new_from_function_cstr(|s| gl_display.get_proc_address(s).cast()) }
                .expect("Cannot create renderer");

        (renderer, window, gl_context, surface)
    };
    let mut canvas = Canvas::new(renderer).expect("Cannot create canvas");

    let mut default_fonts = Fonts {
        default: FontPack {
            default: canvas
                .add_font_mem(&resource!("resources/DejaVuSans.ttf"))
                .expect("Cannot add font"),
            bold: canvas
                .add_font_mem(&resource!("resources/DejaVuSans-Bold.ttf"))
                .expect("Cannot add font"),
            italic: canvas
                .add_font_mem(&resource!("resources/DejaVuSans-Oblique.ttf"))
                .expect("Cannot add font"),
            bold_italic: canvas
                .add_font_mem(&resource!("resources/DejaVuSans-BoldOblique.ttf"))
                .expect("Cannot add font"),
        },

        mono: FontPack {
            default: canvas
                .add_font_mem(&resource!("resources/DejaVuSansMono.ttf"))
                .expect("Cannot add font"),
            bold: canvas
                .add_font_mem(&resource!("resources/DejaVuSansMono-Bold.ttf"))
                .expect("Cannot add font"),
            italic: canvas
                .add_font_mem(&resource!("resources/DejaVuSansMono-Oblique.ttf"))
                .expect("Cannot add font"),
            bold_italic: canvas
                .add_font_mem(&resource!("resources/DejaVuSansMono-BoldOblique.ttf"))
                .expect("Cannot add font"),
        },
    };

    fn set_font<R: Renderer>(
        value: &mut FontId,
        map: &mut IndexMap<Box<str>, Box<str>>,
        name: &str,
        config_path: &Path,
        canvas: &mut Canvas<R>,
    ) {
        let Some(path) = map.shift_remove(name) else {
            return;
        };

        let font_path = config_path.with_file_name(path.as_ref());
        *value = canvas
            .add_font(font_path)
            .unwrap_or_else(|_| panic!("Loading font from {path:?} failed"))
    }

    let path = path.as_ref();

    set_font(
        &mut default_fonts.default.default,
        &mut config_map,
        "Font:default",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.default.bold,
        &mut config_map,
        "Font:default-bold",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.default.italic,
        &mut config_map,
        "Font:default-italic",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.default.bold_italic,
        &mut config_map,
        "Font:default-bold-italic",
        path,
        &mut canvas,
    );

    set_font(
        &mut default_fonts.mono.default,
        &mut config_map,
        "Font:mono",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.mono.bold,
        &mut config_map,
        "Font:mono-bold",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.mono.italic,
        &mut config_map,
        "Font:mono-italic",
        path,
        &mut canvas,
    );
    set_font(
        &mut default_fonts.mono.bold_italic,
        &mut config_map,
        "Font:mono-bold-italic",
        path,
        &mut canvas,
    );

    let mut player_settings = PlayerSettings {
        colors: ColorSettings::common(),
        timing: TimingSettings::common(),
        images: ImageSettings::common(),
        objects: ObjectSettings::common(),
        layout: LayoutSettings::common(),
        names: Names::new(),
    };

    let mut layers = HashMap::new();

    let keys: Vec<_> = config_map.keys().cloned().collect();
    for key in keys {
        let Some(("Layer", name)) = key.split_once(':') else {
            continue;
        };

        let value = config_map.shift_remove(&key).expect("Invalid layer");
        if !value.is_empty() {
            eprintln!("Layers don't accept arguments!");
        }
        layers.insert(name.into(), layers.len());
    }

    if layers.is_empty() {
        layers.insert("Background".into(), 0);
        layers.insert("Character".into(), 1);
    }

    let mut settings_context = SettingsContext {
        object_cache: HashMap::new(),
        layers,
    };

    for (key, value) in config_map {
        if let Some(parameter) = Parameter::create(&key, &mut settings_context) {
            let setter = parameter.value_setter(&value, &settings_context);
            player_settings.set_character_default(setter);
        }
    }

    let (simulation, texts) = T::load(simulation_paths, &story_path, &mut settings_context);

    let player = Player::new(
        (width as f32, height as f32),
        simulation,
        texts,
        save_path,
        player_settings,
    );

    gapp_winit::run(
        player,
        event_loop,
        60,
        context,
        surface,
        window,
        ModifiersState::empty(),
        RenderContext::new(canvas, image_path, default_fonts),
    );
}
