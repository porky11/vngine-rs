use simple_color::Color;

pub fn lerp(a: f32, b: f32, ratio: f32) -> f32 {
    a * (1.0 - ratio) + b * ratio
}

pub fn color(color: Color) -> femtovg::Color {
    femtovg::Color::rgba(color.r, color.g, color.b, color.a)
}
